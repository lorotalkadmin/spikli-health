const {promisify} = require('util')
const axios = require('axios');
const Koa = require('koa');
const nodemailer = require('nodemailer')
const mg = require('nodemailer-mailgun-transport')
const {
  EMAIL_DOMAIN = 'xx',
  EMAIL_TOKEN = 'xx',
  VALIDATION_INTERVAL
} = process.env;
const TIME_INTERVAL = 1000 * VALIDATION_INTERVAL;
const auth = {
  auth: {
    api_key: EMAIL_TOKEN,
    domain: EMAIL_DOMAIN
  }
}
const app = new Koa();
const transporter = nodemailer.createTransport(mg(auth));
const _sendMail = promisify(transporter.sendMail).bind(transporter);

validate();
setInterval(validate, TIME_INTERVAL);

async function validate () {
  let error;
  try {
    const {status, data} = await axios('https://users.spikli.me/health')
    if (status !== 200) {
      error = 'status - ' + status
    }
    else if (!(data instanceof Array)){
      error = 'health missing array'
    }
    else if (!data[0]) {
      error = 'health missing spikli db'
    }
    else if (!data[1]) {
      error = 'health missing jobqueue db'
    }
    else if (data[0] !== 12) {
      error = 'health missing spikli table - ' + data[0]
    }
    else if (data[1] !== 7) {
      error = 'health missing jobqueue db - ' + data[1]
    }
  }
  catch (e) {
    error = "thrown error - " + e.message;
  }
  if (error) {
    console.error(error);
    sendMail(error);
  }
}

function sendMail(error) {
  try {
    return _sendMail({
      from: 'no-reply@spikli.com',
      to: 'sagiv@lorotalk.com',
      subject: 'Zeit health',
      html: error
    });
  }
  catch (e) {
    console.error(e);
  }
}

// response
app.use(ctx => {
  ctx.body = "hey het";
});

app.listen(3000);